import unittest

from AntGame import Coordinate

"""Test Cases for Helper Classes like Coordinate"""


class CoordinateTestCase(unittest.TestCase):

    def test_equal_function(self):

        c1 = Coordinate(0, 0)
        c2 = Coordinate(0, 0)

        # c1 and c2 are equal values, but not the same object
        self.assertEqual(c1, c2)
        self.assertFalse(c1 is c2)

        c1 = c2
        # c1 and c2 points to the same object
        self.assertEqual(c1, c2)
        self.assertTrue(c1 is c2)

    def test_north_function(self):
        c1 = Coordinate(0, 0)

        n1 = c1.north()
        n2 = c1.north()

        # n1 and n2 are equal values, but not the same object
        self.assertEqual(n1, n2)
        self.assertFalse(n1 is n2)

    def test_move_function(self):
        p1 = Coordinate(5, 5)
        p2 = Coordinate(6, 5)
        p1.move(1, 0)
        self.assertEqual(p1, p2)
        self.assertFalse(p1 is p2)


if __name__ == '__main__':
    unittest.main()
