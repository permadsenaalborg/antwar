from AntGame import AntEngine, make_food_map
from Ants import DemoAnt
from GuiGame import GuiGame

my_height = 30
my_width = 20
my_food = make_food_map(my_width, my_height)

my_food[12][14] = 100

step = True
ae = AntEngine([DemoAnt] * 1, training=True, width=my_width, height=my_height, startants=1, food=my_food)


gui = GuiGame(ae, 40)
if step:
    gui.step()
    gui.mainloop()
else:
    gui.play()
