
# Maximum number of ants in one square
MaxSquareAnts = 100

# Maximum number of food in one square
MaxSquareFood = 200

# Number of ant required to build a new base
NewBaseAnts = 25

# Number of food required to build a new base
NewBaseFood = 50

# Point for one base
BaseValue = 50


TimeOutTurn = 20000
WinPercent = 75
NumBattles = 100

MinMapSize = 250
MaxMapSize = 500

RaiseException = True
