docker run -it ^
    --name my_python_antwar ^
    --rm ^
    -v %cd%/results/:/code/results ^
    python_antwar %1 %2 %3 %4 %5 %6
     
pause
