import unittest

from AntGame import AntEngine, Coordinate
from Ants import KillerAnt, NullAnt


class KillerAntTestCase(unittest.TestCase):

    def test_kill_single_enemy(self):

        m = AntEngine([], startants=0, width=10, height=10, training=True)

        enemy1 = NullAnt(m, Coordinate(3, 3), 2)
        ant = KillerAnt(m, Coordinate(4, 3), 1)
        m.print_map()

        m.next()
        m.print_map()

        # KillerAnt one
        self.assertEqual(m.score()[ant.type()], 1)

        # Null ant zero
        self.assertEqual(m.score()[enemy1.type()], 0)

    def test_kill_enemy_ensure_most_damage_right(self):

        m = AntEngine([], startants=0, width=10, height=10, training=True)

        # one enemy on one sie
        enemy1 = NullAnt(m, Coordinate(3, 3), 2)
        ant = KillerAnt(m, Coordinate(4, 3), 1)

        # two on the other side
        NullAnt(m, Coordinate(5, 3), 2)
        NullAnt(m, Coordinate(5, 3), 2)
        m.print_map()

        m.next()
        m.print_map()

        # Killer one
        self.assertEqual(m.score()[ant.type()], 1)

        # ensure we hit the two enemies - one remaining
        self.assertEqual(m.score()[enemy1.type()], 1)

    def test_kill_enemy_ensure_most_damage_left(self):

        m = AntEngine([], startants=0, width=10, height=10, training=True)

        # one enemy on one sie
        enemy1 = NullAnt(m, Coordinate(5, 3), 2)
        ant = KillerAnt(m, Coordinate(4, 3), 1)

        # two on the other side
        NullAnt(m, Coordinate(3, 3), 2)
        NullAnt(m, Coordinate(3, 3), 2)
        m.print_map()

        m.next()
        m.print_map()

        # Killer one
        self.assertEqual(m.score()[ant.type()], 1)

        # ensure we hit the one enemy - two remaining
        self.assertEqual(m.score()[enemy1.type()], 2)


if __name__ == '__main__':
    unittest.main()
