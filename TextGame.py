from Ants import DemoAnt, KillerAnt, CrossDemoAnt, CrossAnt
import AntDef
from Tournament import Tournament

# It is possible to overrule defined values
AntDef.NumBattles = 10
AntDef.TimeOutTurn = 1000
AntDef.RaiseException = False

t = Tournament([DemoAnt, KillerAnt, CrossDemoAnt, CrossAnt])
t.play()
t.print()
