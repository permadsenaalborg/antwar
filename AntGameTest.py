import unittest

import AntDef
from AntGame import AntEngine, AntBase, Coordinate, AlreadyMovedException
from Ants import CheatAntPass, CheatAntDouble, NullAnt,\
    DemoAnt, CrossDemoAnt, CrossAnt


class AntEngineTestCase(unittest.TestCase):

    def setUp(self):
        # reset counters

        NullAnt.Counter = 0

    def test_move_no_kill(self):
        m = AntEngine([AntBase, AntBase], startants=0, training=True)

        self.assertEqual(m.number_of_bases(), 2)

        a1 = AntBase(m, Coordinate(10, 10), 1)

        # we can create an ant with keeping an explicit reference
        AntBase(m, Coordinate(20, 20), 2)

        self.assertEqual(m.number_of_bases(), 2)

        # two ant objects
        self.assertEqual(len(m.all()), 2)

        m.move(a1, -1, 0, False)
        m.move(a1, -1, 0, False)
        m.move(a1, -1, 0, False)
        m.move(a1, -1, 0, False)
        m.move(a1, -1, 0, False)

        # still two ant objects?
        self.assertEqual(len(m.all()), 2)

    def test_move_one_kill(self):
        m = AntEngine([AntBase, AntBase], startants=0, training=True)

        a1 = AntBase(m, Coordinate(10, 10), 1)
        a2 = AntBase(m, Coordinate(12, 10), 2)

        # two ant objects
        self.assertEqual(len(m.all()), 2)

        m.move(a2, -1, 0, False)
        m.move(a2, -1, 0, False)

        self.assertEqual(len(m.all()), 1)

        # ant object still exists, but may not be part of the game
        self.assertIsNotNone(a1)
        self.assertIsNotNone(a2)

    def test_move_around_map(self):
        m = AntEngine([AntBase], startants=0, width=10, height=10, training=True)

        a1 = AntBase(m, Coordinate(8, 8), 1)
        m.move(a1, 1, 0, False)
        m.move(a1, 1, 0, False)
        self.assertEqual(a1._Position, Coordinate(0, 8))

        m.move(a1, 0, 1, False)
        m.move(a1, 0, 1, False)
        self.assertEqual(a1._Position, Coordinate(0, 0))

    def test_new_ant(self):
        food_map = [[0 for y in range(10)] for x in range(10)]

        food_map[4][4] = 10
        m = AntEngine([AntBase], training=True, startants=0, width=10, height=10, food=food_map)
        self.assertEqual(len(m.all()), 0)

        a1 = AntBase(m, Coordinate(5, 5), 1)
        m.move(a1, -1, 0, False)
        m.move(a1, 0, -1, False)
        self.assertTrue(a1._Position == Coordinate(4, 4))
        m.print_map()

        m.move(a1, 1, 0, True)
        m.move(a1, 0, 1, True)
        self.assertTrue(a1._Position == Coordinate(5, 5))
        m.print_map()

        m.next()
        m.print_map()
        self.assertEqual(len(m.all()), 2)

    def test_double_move(self):
        m = AntEngine([AntBase, AntBase], startants=0, training=True)

        c = CheatAntDouble(m, Coordinate(20, 20), 2)

        self.assertIsNotNone(c)
        with self.assertRaises(AlreadyMovedException):
            m.next()

    def test_no_move(self):
        m = AntEngine([AntBase, AntBase], startants=0, training=True)

        CheatAntPass(m, Coordinate(20, 20), 2)

        with self.assertRaises(Exception):
            m.next()

    def test_play_game_one_ant(self):
        m = AntEngine([AntBase], startants=5, width=10, height=10, training=True)
        m.play()

        # AntBase is alone => it must win immediately

        self.assertEqual(m.winner(), "AntBase 1")
        self.assertEqual(m.round_no, 0)

    def test_play_game_two_ants(self):
        m = AntEngine([AntBase, AntBase], startants=5, width=10, height=10, training=True)
        m.play()
        m.print_map()

        self.assertEqual(m.round_no, AntDef.TimeOutTurn)

    def test_build_base(self):
        food_map = [[0 for y in range(10)] for x in range(10)]

        food_map[8][8] = AntDef.NewBaseFood
        m = AntEngine([AntBase], startants=0, width=10, height=10, training=True, food=food_map)

        ant = None
        for idx in range(AntDef.NewBaseAnts):
            ant = AntBase(m, Coordinate(8, 8), 1)

        self.assertEqual(m.number_of_bases(), 1)

        m.print_map()
        ant.build_base()

        self.assertEqual(m.number_of_bases(), 2)

        m.print_map()
        self.assertEqual(food_map[5][5], 0)

    def test_kill_base(self):

        m = AntEngine([AntBase], startants=0, width=10, height=10, training=True)
        n1 = NullAnt(m, Coordinate(4, 4), 2)

        # one ant objects
        self.assertEqual(len(m.all()), 1)
        self.assertEqual(m.number_of_bases(), 1)

        m.print_map()

        m.move(n1, 1, 0, False)
        m.print_map()
        m.move(n1, 0, 1, False)

        self.assertEqual(len(m.all()), 1)
        self.assertEqual(m.number_of_bases(), 0)

        m.print_map()

    def test_map_size(self):

        # map size is based on random data, so we test 10 times
        for i in range(10):
            m = AntEngine([AntBase])
            self.assertEqual(m.MapHeight % 64, 0)
            self.assertEqual(m.MapWidth % 64, 0)

            self.assertLess(m.MapHeight, AntDef.MaxMapSize, 0)
            self.assertLess(m.MapWidth, AntDef.MaxMapSize, 0)

            self.assertGreater(m.MapHeight, AntDef.MinMapSize, 0)
            self.assertGreater(m.MapWidth, AntDef.MinMapSize, 0)

    def test_empty_antgame(self):
        m = AntEngine([])
        self.assertIsNotNone(m)

    def test_ant_ids(self):
        # simple setup
        m = AntEngine([])
        c = Coordinate(0, 0)

        # check IDs
        n1 = NullAnt(m, c, 1)
        self.assertEqual(n1.ID, 0)

        n2 = NullAnt(m, c, 1)
        self.assertEqual(n2.ID, 1)

        n3 = NullAnt(m, c, 1)
        self.assertEqual(n3.ID, 2)

        d1 = DemoAnt(m, c, 1)
        self.assertEqual(d1.ID, 0)

        d2 = DemoAnt(m, c, 1)
        self.assertEqual(d2.ID, 1)

        d3 = DemoAnt(m, c, 1)
        self.assertEqual(d3.ID, 2)

    def test_CrossDemoAnt(self):

        ae = AntEngine([CrossDemoAnt, NullAnt, CrossAnt], 0)
        my_ants = []
        for ants in range(5):
            my_ants.append(CrossDemoAnt(ae, Coordinate(0, 0), 1))
            NullAnt(ae, Coordinate(20, 20), 2)

        for t in range(100):
            ae.next()
            for ant in my_ants:
                self.assertEqual(ant.step, t+1)


if __name__ == '__main__':
    unittest.main()
