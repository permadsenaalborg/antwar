# Print some basic info for debugging purpose
pwd
ls -l
export PYTHONPATH="$PYTHONPATH:."
python -c "import sys;print(sys.path)"


# actually perform test
nosetests --with-html --html-file=Tests.html AntGameTest.py KillerAntTest.py HelperTest.py