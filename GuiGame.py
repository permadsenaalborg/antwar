import tkinter as tk
from AntGame import Ant_Colors
import time
from tkinter import messagebox


class GuiGame:
    def __init__(self, engine, cell_size=None):

        if cell_size is None:
            self.cell_size = 800 // engine.MapWidth
        else:
            self.cell_size = cell_size

        self.ae = engine
        self.root = tk.Tk()
        self.master = tk.Frame(self.root)
        self.master.pack()

        self.top_frame = tk.Frame(self.master)
        self.top_frame.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)

        self.left_frame = tk.Frame(self.top_frame, borderwidth=5)
        self.left_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)

        self.right_frame = tk.Frame(self.top_frame, borderwidth=5, width=250)
        self.right_frame.pack(side=tk.RIGHT, fill=tk.BOTH, expand=tk.YES)

        self.cancel = False
        self.root.title("Ant Game")

        self.w = tk.Canvas(self.left_frame, width=self.c2p(self.ae.MapWidth), height=self.c2p(self.ae.MapHeight))

        self.w.bind("<Button-1>", self.clicked)
#        p = Button(self.right_frame, text="Pause", fg="red", command=self.pause)
        s = tk.Button(self.right_frame, text="Step", fg="red", command=self.step)
        s10 = tk.Button(self.right_frame, text="Step 10", fg="red", command=self.step_helper10)
        q = tk.Button(self.right_frame, text="QUIT", fg="red", command=self.close)
#        p.pack(side="top")
        s.pack(side="top")
        s10.pack(side="top")
        q.pack(side="bottom")

        self.scores = dict()
        for team, ants in sorted(engine.score().items()):

            name, idx = team.split(" ")
            my_label = tk.Label(self.right_frame, text=team + ": " + str(ants),  bg=Ant_Colors[int(idx)],  width=22)
            my_label.pack()
            self.scores[str(team)] = my_label

        self.total_label = tk.Label(self.right_frame, text="Total : " + str(engine.totalpoints))
        self.total_label.pack()

        self.root.protocol("WM_DELETE_WINDOW", self.close)
        self.w.pack()

    def clicked(self, event):
        x = event.x // self.cell_size
        y = event.y // self.cell_size

        print("x: " + str(x) + ", y:" + str(y) + " " + self.ae.info(x, y))

    def c2p(self, c):
        return c * self.cell_size

    def pause(self):
        time.sleep(5)

    def close(self):
        self.cancel = True
        self.master.destroy
        self.master.quit()

#    def step_helper(self):
#        self.step(1)

    def step_helper10(self):
        for t in range(10):
            self.step()

    def step(self):
        self.ae.next()
        self.draw()
        self.root.title("Round:" + str(self.ae.round_no))
        for team, ants in sorted(self.ae.score().items()):
            self.scores[str(team)]['text'] = str(team) + ": " + str(ants)
            # print(str(team) + ": " + str(ants))

        self.total_label['text'] = "Total : " + str(self.ae.totalpoints)
        self.w.update()

    def draw(self):
        self.ae.draw_canvas(self.w, self.cell_size)

    def play(self):
        while not self.ae.winner():
            if self.cancel:
                break
            self.step()
        self.show_winner()

    def show_winner(self):
        winner = self.ae.winner()
        if winner:
            messagebox.showinfo("The winner is:", str(winner) + self.ae.scoreText())

    def mainloop(self):
        tk.mainloop()
