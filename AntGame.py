import AntDef
import sys
from collections import defaultdict
from random import randrange, shuffle

Ant_Colors = ['black', 'red', 'green', 'blue', 'cyan', 'yellow', "#FF00FF", "#AAAAAA"]


class AlreadyMovedException(Exception):
    def __init__(self):
        super().__init__("Ant already moved")


class SquareData:
    def __init__(self):
        self.NumAnts = 0
        self.Base = False
        self.Team = 0
        self.NumFood = 0

    def __str__(self):
        return "Team : " + str(self.Team) + ", NumAnts: " + str(self.NumAnts) +\
               ", NumFood: " + str(self.NumFood) + ", Base: " + str(self.Base)


class ScopeData:
    def __init__(self):
        self.Center = SquareData()
        self.North = SquareData()
        self.South = SquareData()
        self.East = SquareData()
        self.West = SquareData()

    def __str__(self):
        return "center: " + str(self.Center)


class AntHome:

    def __init__(self, pos, cls, idx):
        self.pos = pos
        self.cls = cls
        self.idx = idx

    def type(self):
        return self.cls.__name__ + " " + str(self.idx)


class Coordinate:
    def __init__(self, x=0, y=0):
        self.X = x
        self.Y = y

    def move(self, dx, dy):
        self.X += dx
        self.Y += dy

    def center(self):
        return Coordinate(self.X, self.Y)

    def south(self):
        return Coordinate(self.X, self.Y+1)

    def north(self):
        return Coordinate(self.X, self.Y-1)

    def north_fail(self):
        if sys.version_info[0] == 3 and sys.version_info[1] == 6:
            return Coordinate(self.X, self.Y-1)
        else:
            return None

    def east(self):
        return Coordinate(self.X+1, self.Y)

    def west(self):
        return Coordinate(self.X-1, self.Y)

    def __str__(self):
        return str(self.X) + ', ' + str(self.Y)

    def __eq__(self, other):
        return self.X == other.X and self.Y == other.Y


def make_food_map(x_size, y_size):
    return [[0 for y in range(y_size)] for x in range(x_size)]


class AntEngine:

    def __init__(self, players, startants=None, training=False, height=None, width=None, food=None):

        if width is None:
            factor = randrange(4, 8)
            self.MapWidth = 64 * factor
        else:
            self.MapWidth = width

        if height is None:
            factor = randrange(4, 8)
            self.MapHeight = 64 * factor
        else:
            self.MapHeight = height

        if startants is None:
            self.StartAnts = randrange(10, 50)
        else:
            self.StartAnts = startants

        self.NewFoodSpace = randrange(15, 40)
        self.NewFoodMin = randrange(10, 30)
        self.NewFoodDiff = randrange(5, 20)
        self.MakeFood = True

        if food is None:
            self.__Food = make_food_map(self.MapWidth, self.MapHeight)
        else:
            self.__Food = food
            self.MakeFood = False

        self.__data = [[[] for y in range(self.MapHeight)] for x in range(self.MapWidth)]
        self.__killed = []
        self.__homes = []
        self.cancel = False

        self._score = defaultdict(int)
        self.totalpoints = 100000

        self.round_no = 0
        self.training = training

        for count, p in enumerate(players):
            if training and len(players) == 1:
                ant_base = self.make_center_pos()
            else:
                ant_base = self.make_random_pos(True)
            for a in range(self.StartAnts):
                p(self, ant_base, count+1)

            self.__homes.append(AntHome(ant_base, p, count+1))
            self.score()

    def all(self):
        res = []
        for w in range(self.MapWidth):
            for h in range(self.MapHeight):
                res.extend(self.__data[w][h])
        return res

    def add(self, x, y, ant):
        who = self.getIndex(x, y)

        if who != 0 and who != ant.Index:
            self.__killed.extend(self.__data[x][y])
            self.__data[x][y] = [ant]
        else:
            self.__data[x][y].append(ant)

        to_delete = None
        for h in self.__homes:
            if h.pos == Coordinate(x, y) and h.idx != ant.Index:
                print("We hit a base!")
                to_delete = h

        if to_delete is not None:
            self.__homes.remove(to_delete)

    def number_of_bases(self):
        return len(self.__homes)

    def remove(self, x, y, ant):
        assert ant in self.__data[x][y], str(ant)
        self.__data[x][y].remove(ant)

    def print(self, x, y):
        res = ""
        for a in self.__data[x][y]:
            res += str(a) + ", "
        return res

    def buildbase(self, pos, cls, idx):
        for h in self.__homes:
            if h.pos == pos:
                print("Can't build base here!")
                return

        if self.__Food[pos.X][pos.Y] >= AntDef.NewBaseFood and len(self.__data[pos.X][pos.Y]) >= AntDef.NewBaseAnts:
            print("Build base")
            self.__homes.append(AntHome(pos.center(), cls, idx))
            self.__Food[pos.X][pos.Y] -= AntDef.NewBaseFood

    def move(self, ant, dx, dy, withfood):
        assert (abs(dx) + abs(dy) == 1)

        if ant._Position.X + dx > self.MapWidth - 1:
            dx = -self.MapWidth + 1

        elif ant._Position.Y + dy > self.MapHeight - 1:
            dy = -self.MapHeight + 1

        elif ant._Position.X + dx < 0:
            dx = self.MapWidth - 1

        elif ant._Position.Y + dy < 0:
            dy = self.MapHeight - 1

        self.remove(ant._Position.X, ant._Position.Y, ant)

        food_avail = self.__Food[ant._Position.X][ant._Position.Y] > 0

        if withfood and food_avail:
            self.__Food[ant._Position.X][ant._Position.Y] -= 1

        ant._Position.X += dx
        ant._Position.Y += dy

        self.add(ant._Position.X, ant._Position.Y, ant)

        if withfood and food_avail:
            self.__Food[ant._Position.X][ant._Position.Y] += 1

    def draw_canvas(self, c, cell_size):
        if self.cancel:
            return

        c.delete("all")
        for w in range(self.MapWidth):
            for h in range(self.MapHeight):
                i = self.getIndex(w, h)
                x = w*cell_size
                y = h*cell_size
                if i > 0:
                    c.create_rectangle(x, y, x+cell_size, y+cell_size, fill=Ant_Colors[i % 8])
                # else:
                # c.create_rectangle(x, y, x+cell_size, y+cell_size, fill='white')

                foods = self.__Food[w][h]
                if foods > 0:
                    food_size = foods
                    if food_size > cell_size - 2:
                        food_size = cell_size - 2
                    delta = (cell_size - food_size)/2
                    fx1 = x + delta
                    fy1 = y + delta
                    fx2 = fx1 + food_size
                    fy2 = fy1 + food_size

                    c.create_oval(fx1, fy1, fx2, fy2, fill='#AAAAAA')

        for h in self.__homes:
            c.create_text((h.pos.X+0.5)*cell_size, (h.pos.Y+0.5)*cell_size,
                          fill="darkblue", font="Times 12 italic bold", text="H")

    def getIndex(self, x, y):
        a = self.__data[x][y]
        if len(a) > 0:
            res = a[0].Index
        else:
            res = 0
        return res

    def get_number(self, x, y):
        return len(self.__data[x][y])

    def info(self, x, y):
        square_info = str(self.create_square(Coordinate(x, y)))
        ant_info = ""
        for a in self.__data[x][y]:
            ant_info += "\n\t" + str(a)
        return square_info + "\n" + ant_info

    def create_square(self, pos):
        sq = SquareData()

        sq.Team = self.getIndex(pos.X, pos.Y)
        sq.NumFood = self.__Food[pos.X][pos.Y]
        sq.NumAnts = len(self.__data[pos.X][pos.Y])

        for h in self.__homes:
            if h.pos == pos:
                sq.Base = True
                # hack hide food in base
                sq.NumFood = 0
                sq.Team = h.idx

        return sq

    def create_scope(self, pos):
        sc = ScopeData()
        sc.Center = self.create_square(pos)

        if pos.Y < self.MapHeight - 1:
            sc.South = self.create_square(pos.south())

        if pos.Y > 0:
            sc.North = self.create_square(pos.north())

        if pos.X < self.MapWidth - 1:
            sc.East = self.create_square(pos.east())

        if pos.X > 0:
            sc.West = self.create_square(pos.west())

        return sc

    def make_random_pos(self, avoid_border=False):
        if avoid_border:
            dx = self.MapWidth // 20
            dy = self.MapHeight // 20
            return Coordinate(randrange(dx, self.MapWidth-dx), randrange(dy, self.MapHeight-dy))
        else:
            return Coordinate(randrange(0, self.MapWidth), randrange(0, self.MapHeight))

    def make_center_pos(self):
        return Coordinate(self.MapWidth // 2, self.MapHeight // 2)

    def bases(self):
        res = []
        for h in self.__homes:
            res.append(h.pos)
        return res

    def make_new_food(self):
        coordinate = None

        map_size = self.MapWidth * self.MapHeight
        if self.MakeFood and (self.training or (self.totalpoints < map_size / self.NewFoodSpace)):
            while coordinate is None or coordinate in self.bases():
                coordinate = self.make_random_pos()

            self.__Food[coordinate.X][coordinate.Y] = randrange(self.NewFoodMin, self.NewFoodMin + self.NewFoodDiff)

    def make_new_ants(self):
        for h in self.__homes:
            while self.__Food[h.pos.X][h.pos.Y] > 0:
                h.cls(self, h.pos, h.idx)
                self.__Food[h.pos.X][h.pos.Y] -= 1

    def winner(self):
        if self.round_no < AntDef.TimeOutTurn:
            for team, points in self.score().items():
                if points > (self.totalpoints * AntDef.WinPercent / 100):
                    return team
            else:
                return False
        else:
            max_points = 0
            win_team = None
            for team, points in self.score().items():
                if points > max_points:
                    max_points = self._score[team]
                    win_team = team
            return win_team

    def play(self):
        while not self.winner():
            self.next()
        return self.winner()

    def next(self):
        self.round_no += 1
        all_ant = self.all()

        shuffle(all_ant)
        self.__killed = []
        for a in all_ant:
            if a in self.__killed:
                # print(str(a) + ", I was killed, no move")
                break
            else:
                a.clear()
                mates = list(self.__data[a._Position.X][a._Position.Y])
                mates.remove(a)
                sc = self.create_scope(a._Position)
                a.move(sc, mates)
                if not a.moved():
                    if AntDef.RaiseException:
                        raise Exception("Please move Ant: " + str(a))
                    else:
                        print("Please move Ant: " + str(a))
                        a.remove_me()
                        # self.remove(a._Position.x, a._Position.y, a)

        self.make_new_ants()
        self.make_new_food()

    def score(self):
        self._score = defaultdict(int)

        for a in self.all():
            self._score[a.type()] += 1

        for h in self.__homes:
            self._score[h.type()] += AntDef.BaseValue

        self.totalpoints = 0
        for team in self._score:
            self.totalpoints += self._score[team]

        return self._score

    def scoreText(self):
        res = ""
        for team, ants in sorted(self._score.items()):
            res += f"Team {team}: {ants}, "
        return res

    def __str__(self):
        return "Map = " + str(self.MapWidth) + "x" + str(self.MapHeight)

    def print_map(self):

        print("Map:")
        print("Ant:\t\tNumber:")
        for h in reversed(range(self.MapHeight)):
            line = ""
            for w in range(self.MapWidth):
                i = self.getIndex(w, h)
                if i > 0:
                    line += str(i)
                else:
                    if self.__Food[w][h] > 0:
                        line += "*"
                    else:
                        line += "."

            line += "\t"
            for w in range(self.MapWidth):
                i = self.get_number(w, h)
                if i > 0:
                    line += str(i)
                else:
                    line += "."

            print(line)


class AntBase:
    Counter = 0

    def __init__(self, engine, start_position, idx):
        self._Position = Coordinate(start_position.X, start_position.Y)
        self.Index = idx
        self.ID = type(self).Counter
        self._moved = False

        type(self).Counter += 1
        self.__Engine = engine
        self.__Engine.add(start_position.X, start_position.Y, self)
        # to be removed
        self.ant_init()

    def remove_me(self):
        self.__Engine.remove(self._Position.X, self._Position.Y, self)

    def already_moved(self):
        if AntDef.RaiseException:
            raise AlreadyMovedException()
        else:
            print(f"Ant was already moved: {self.ID}")
            self.remove_me()

    def ant_init(self):
        """Put your Ants initialization stuff here"""
        pass

    def __str__(self):
        return self.type() + "/" + str(self.ID) + ": " + str(self._Position)

    def type(self):
        return str(self.__class__.__name__) + " " + str(self.Index)

    def move(self, scope, mates):
        """Method that all Ants should override"""
        self.stay()

    def clear(self):
        self._moved = False

    def moved(self):
        return self._moved

    def stay(self):
        if self._moved:
            self.already_moved()
            return
        self._moved = True

    def move_delta(self, dx, dy, with_food):
        if self._moved:
            self.already_moved()
            return
        self.__Engine.move(self, dx, dy, with_food)
        self._moved = True

    def north(self, with_food=False):
        self.move_delta(0, -1, with_food)

    def south(self, with_food=False):
        self.move_delta(0, 1, with_food)

    def east(self, with_food=False):
        self.move_delta(1, 0, with_food)

    def west(self, with_food=False):
        self.move_delta(-1, 0, with_food)

    def build_base(self):
        if self._moved:
            self.already_moved()
            return
        self._moved = True
        self.__Engine.buildbase(self._Position, self.__class__, self.Index)
