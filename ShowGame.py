from AntGame import AntEngine
from Ants import DemoAnt, KillerAnt, MathDemoAnt, CrossAnt, CrossDemoAnt
from GuiGame import GuiGame

# Create an engine for playing the game, with two ants, set width and height
ae = AntEngine([MathDemoAnt,
                DemoAnt,
                KillerAnt,
                CrossAnt,
                CrossDemoAnt], width=250, height=250, startants=90)

# Create a graphical interface, set pixel-size to 4
gui = GuiGame(ae, 4)

# go a head - play
gui.play()
