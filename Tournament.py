import logging
from collections import defaultdict
import time

from AntGame import AntEngine
import AntDef

# It is possible to overrule defined values
# AntDef.NumBattles = 10
# AntDef.TimeOutTurn = 1000
AntDef.RaiseException = False


class Tournament:

    def __init__(self, ants):
        self.winner = defaultdict(int)
        self.start_time = time.time()
        logging.basicConfig(filename="antgame_results.log", filemode="w", level=logging.INFO)
        self.ants = ants

    def play(self):
        for t in range(AntDef.NumBattles):
            logging.info("")
            logging.info(f"Game #{t+1}")
            engine = AntEngine(self.ants)
            engine.play()
            logging.info("Score: " + engine.scoreText())
            logging.info("Winner: " + engine.winner())
            self.winner[engine.winner()] += 1

    def print(self):
        logging.info("")
        time_used = (time.time() - self.start_time)
        logging.info(f"Final results, games: {AntDef.NumBattles}, time: {time_used:.2f}")
        logging.info("=======================")
        for t, wins in sorted(self.winner.items(), reverse=False):
            logging.info(t + ":\t" + str(wins))
