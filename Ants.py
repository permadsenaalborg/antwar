"""This file includes the source for the following ants:

Playable ants:

DemoAnt
KillerAnt
CrossDemoAnt
MathDemoAnt
NullAnt

For testing / demonstration

CheatAntDouble
CheatAntPass
"""
import math
from random import randrange

from AntGame import AntBase


class NullAnt(AntBase):

    def __init__(self, *args):
        super().__init__(*args)

    def move(self, scope, mates):
        self.stay()


class DemoAnt(AntBase):

    def __init__(self, *args):
        super().__init__(*args)

    def move(self, scope, mates):

        # print("I am: " + str(self))
        # for m in mates:
        #   print("\tI know: " + str(m))

        x = randrange(4)
        if x == 0:
            self.east(True)
        elif x == 1:
            self.west(True)
        elif x == 2:
            self.north(True)
        elif x == 3:
            self.south(True)


class KillerAnt(DemoAnt):

    def move(self, scope, mates):
        if scope.North.NumAnts > 0 and scope.North.Team != self.Index:
            self.north()
        elif scope.South.NumAnts > 0 and scope.South.Team != self.Index:
            self.south()
        elif scope.East.NumAnts > 0 and scope.East.Team != self.Index:
            self.east()
        elif scope.West.NumAnts > 0 and scope.West.Team != self.Index:
            self.west()
        else:
            super().move(scope, mates)


class CrossAnt(AntBase):

    def __init__(self, *args):
        super().__init__(*args)
        self.direction = randrange(4)
        self.step = 0
        self.step_limit = 5 + (self.ID % 50)

    def move(self, scope, mates):
        if self.step % self.step_limit == 0:
            if self.direction == 0:
                self.direction = 3
            elif self.direction == 3:
                self.direction = 1
            elif self.direction == 1:
                self.direction = 2
            elif self.direction == 2:
                self.direction = 0
        self.step += 1

        if self.direction == 0:
            self.east(True)
        elif self.direction == 1:
            self.west(True)
        elif self.direction == 2:
            self.north(True)
        elif self.direction == 3:
            self.south(True)

    def __str__(self):
        return super().__str__() + " Direction: " + str(self.direction) + " Step: " + str(self.step)


class CheatAntDouble(AntBase):
    """Ant that tries to move twice"""

    def move(self, scope, mates):
        self.south()
        self.south()


class CheatAntPass(AntBase):
    """Ant that never moves"""
    def move(self, scope, mates):
        pass


class CrossDemoAnt(AntBase):

    def __init__(self, *args):
        super().__init__(*args)
        self.direction = randrange(4)
        self.step = 0

    def update_direction(self):
        if self.step % 10 == 0:
            if self.direction == 0:
                self.direction = 1
            elif self.direction == 1:
                self.direction = 0
            elif self.direction == 2:
                self.direction = 3
            elif self.direction == 3:
                self.direction = 2
        self.step += 1

    def move_in_direction(self):
        if self.direction == 0:
            self.east(True)
        elif self.direction == 1:
            self.west(True)
        elif self.direction == 2:
            self.north(True)
        elif self.direction == 3:
            self.south(True)

    def move(self, scope, mates):
        self.update_direction()
        self.move_in_direction()


class MathDemoAnt(AntBase):

    def __init__(self, *args):
        super().__init__(*args)
        self.pos_x = 0
        self.pos_y = 0
        # our goal position
        self.goal_x = 0
        self.goal_y = 0
        # calc angle from ID
        self.angle_r = math.radians((self.ID * 4) % 360)

    def east(self):
        self.pos_x += 1
        super().east(True)

    def west(self):
        self.pos_x -= 1
        super().west(True)

    def north(self):
        self.pos_y += 1
        super().north(True)

    def south(self):
        self.pos_y -= 1
        super().south(True)

    def move_to_goal(self):
        dx = self.goal_x - self.pos_x
        dy = self.goal_y - self.pos_y

        if abs(dx) > abs(dy):
            if dx > 0:
                self.east()
            else:
                self.west()
        else:
            if dy > 0:
                self.north()
            else:
                self.south()

    def move(self, scope, mates):
        if self.pos_x == self.goal_x and self.pos_y == self.goal_y:
            if self.goal_x == 0:
                self.goal_x = int(math.cos(self.angle_r) * 50)
                self.goal_y = int(math.sin(self.angle_r) * 50)
            else:
                # self.stay()
                # return
                self.goal_x = 0
                self.goal_y = 0

        self.move_to_goal()
